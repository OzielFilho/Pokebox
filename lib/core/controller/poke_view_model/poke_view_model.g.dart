// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'poke_view_model.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PokeViewModel on _PokeViewModelBase, Store {
  final _$listPokemonAtom = Atom(name: '_PokeViewModelBase.listPokemon');

  @override
  List<dynamic> get listPokemon {
    _$listPokemonAtom.reportRead();
    return super.listPokemon;
  }

  @override
  set listPokemon(List<dynamic> value) {
    _$listPokemonAtom.reportWrite(value, super.listPokemon, () {
      super.listPokemon = value;
    });
  }

  final _$listObjPokemonAtom = Atom(name: '_PokeViewModelBase.listObjPokemon');

  @override
  ObservableList<Pokemon> get listObjPokemon {
    _$listObjPokemonAtom.reportRead();
    return super.listObjPokemon;
  }

  @override
  set listObjPokemon(ObservableList<Pokemon> value) {
    _$listObjPokemonAtom.reportWrite(value, super.listObjPokemon, () {
      super.listObjPokemon = value;
    });
  }

  final _$urlAtom = Atom(name: '_PokeViewModelBase.url');

  @override
  String get url {
    _$urlAtom.reportRead();
    return super.url;
  }

  @override
  set url(String value) {
    _$urlAtom.reportWrite(value, super.url, () {
      super.url = value;
    });
  }

  final _$retornoAtom = Atom(name: '_PokeViewModelBase.retorno');

  @override
  Map<dynamic, dynamic> get retorno {
    _$retornoAtom.reportRead();
    return super.retorno;
  }

  @override
  set retorno(Map<dynamic, dynamic> value) {
    _$retornoAtom.reportWrite(value, super.retorno, () {
      super.retorno = value;
    });
  }

  final _$golpesListAtom = Atom(name: '_PokeViewModelBase.golpesList');

  @override
  List<List<String>> get golpesList {
    _$golpesListAtom.reportRead();
    return super.golpesList;
  }

  @override
  set golpesList(List<List<String>> value) {
    _$golpesListAtom.reportWrite(value, super.golpesList, () {
      super.golpesList = value;
    });
  }

  final _$typePokemonAtom = Atom(name: '_PokeViewModelBase.typePokemon');

  @override
  List<dynamic> get typePokemon {
    _$typePokemonAtom.reportRead();
    return super.typePokemon;
  }

  @override
  set typePokemon(List<dynamic> value) {
    _$typePokemonAtom.reportWrite(value, super.typePokemon, () {
      super.typePokemon = value;
    });
  }

  final _$respoAPIAsyncAction = AsyncAction('_PokeViewModelBase.respoAPI');

  @override
  Future<void> respoAPI() {
    return _$respoAPIAsyncAction.run(() => super.respoAPI());
  }

  @override
  String toString() {
    return '''
listPokemon: ${listPokemon},
listObjPokemon: ${listObjPokemon},
url: ${url},
retorno: ${retorno},
golpesList: ${golpesList},
typePokemon: ${typePokemon}
    ''';
  }
}
