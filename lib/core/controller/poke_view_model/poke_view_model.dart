import 'dart:async';

import 'package:dio/dio.dart';
import 'package:mobx/mobx.dart';

import '../../models/pokemon/Pokemon.dart';

part 'poke_view_model.g.dart';

class PokeViewModel = _PokeViewModelBase with _$PokeViewModel;

abstract class _PokeViewModelBase with Store {
  _PokeViewModelBase() {
    respoAPI();
  }

  @observable
  var listPokemon = [];

  @observable
  var listObjPokemon = <Pokemon>[].asObservable();

  @observable
  String url = 'https://pokeapi.co/api/v2/pokemon/';

  @observable
  var retorno = {};

  @observable
  List<List<String>> golpesList = [];

  @observable
  List typePokemon = [];

  @action
  Future<void> respoAPI() async {
    var dio = new Dio();
    Response response = await dio.get(url);

    listPokemon = response.data['results'] as List;
    int j;

    for (j = 0; j < listPokemon.length; j++) {
      // Url para recuperar as imagens e os golpes do pokemon especificado pelo j
      String urlForms = 'https://pokeapi.co/api/v2/pokemon-form/${j + 1}';
      String urlGolpes = 'https://pokeapi.co/api/v2/pokemon/${j + 1}';

      // resposta da api juntamente com um map para armazenar o corpo do retorno
      response = await dio.get(urlForms);

      // resposta da api juntamente com um map para armazenar o corpo do retorno
      Response golpes = await dio.get(urlGolpes);

      List<String> golpesRetorno = [];
      // adição do nome dos golpes de cada j(pokemon) a uma lista de strings
      golpesRetorno.add(golpes.data['moves'][0]['move']['name']);
      golpesRetorno.add(golpes.data['moves'][1]['move']['name']);
      golpesRetorno.add(golpes.data['moves'][2]['move']['name']);
      golpesRetorno.add(golpes.data['moves'][3]['move']['name']);
      // adição dessa lista de golpes a uma lista para acessar os pokemons pelo
      // seu indice (j)
      golpesList.add(golpesRetorno);

      //lista dos tipos do pokemon
      typePokemon.add(golpes.data['types']);

      //lista de imagens
      List<String> images = [
        response.data['sprites']['front_default'],
        response.data['sprites']['back_default']
      ];

      // adicao na lista de objetos Pokemon contendo todas as informaçoes do meu
      // pokemon
      listObjPokemon.add(
        Pokemon(
          name: listPokemon[j]['name'],
          urlImage: images,
          golpes: golpesList[j],
          typePokemon: typePokemon[j],
        ),
      );
    }
  }
}
